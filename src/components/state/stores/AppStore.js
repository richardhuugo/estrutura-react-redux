// IMPORT PACKAGE REFERENCES

import { createStore, applyMiddleware } from 'redux';

// IMPORT MIDDLEWARE

import thunk from 'redux-thunk';

import promise from 'redux-promise'
import multi from 'redux-multi'

import promiseMiddleware from 'redux-promise-middleware';

// IMPORT REDUCERS

import { AppReducer } from '../reducers/AppReducer';


// CONFIGURE STORE

export const createAppStore = () => {
    return createStore(AppReducer, applyMiddleware(thunk,multi,  promise, promiseMiddleware()));
};
import { toastr } from 'react-redux-toastr'
import axios from 'axios'
import consts from '../../consts'

export function login(values) {
    return submit(values, `${consts.OAPI_URL}/login-user`)
}

export function signup(values) {
    return submit(values, `${consts.OAPI_URL}/register-user`)
}

function submit(values, url) {
    return dispatch => {
        axios.post(url, values)
            .then(resp => {
                dispatch([
                    { type: 'USER_FETCHED', payload: resp.data }
                ])
            })
            .catch(e => {
                e.response.data.errors.forEach(
                    error => toastr.error('Erro', error))
              
            })
    }
}

export function logout() {
    return { type: 'TOKEN_VALIDATED', payload: null }
}

export function validateToken(token) {
    return dispatch => {
        if (token) {
            axios.post(`${consts.OAPI_URL}/validate`, { token })
                .then(resp => {
                    dispatch({ type: 'TOKEN_VALIDATED', payload: resp.data.valid })
                })
                .catch(e => dispatch({ type: 'TOKEN_VALIDATED', payload: null }))
        } else {
            dispatch({ type: 'TOKEN_VALIDATED', payload: null })
        }
    }
}
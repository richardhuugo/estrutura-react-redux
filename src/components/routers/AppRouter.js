// IMPORT PACKAGE REFERENCES

import React, { Fragment } from 'react';
  
import AuthOrApp from './AuthOrApp'


import {  BrowserRouter } from 'react-router-dom';
import 'jquery'
import 'bootstrap'
import 'popper.js'
import '@coreui/coreui'
import '@coreui/coreui/dist/js/coreui.min'
import '@coreui/coreui/dist/css/coreui.min.css'
import '@coreui/icons/css/coreui-icons.min.css'
//<AuthOrApp />
  
export const AppRouter =() => (
    <BrowserRouter forceRefresh={true} >
        <AuthOrApp />
    </BrowserRouter>

)
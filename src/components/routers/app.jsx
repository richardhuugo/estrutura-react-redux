
import React, { Fragment } from 'react';
import {   Route,   Redirect } from 'react-router-dom';
import axios from 'axios'

const PrivateRouter = ({validToken, user, component:Component, ...rest}) => {
  
  if(validToken || user != null){
    axios.defaults.headers.common['authorization'] = user.token
  }
  return (
    
    <Route 
    {...rest} render={props => (validToken || user ) ?( <Component {...props} />) : ( <Redirect to={{pathname:'/login', state:{from:props.location}}} /> )             
    }
    
    />
  )
} 
  
  export default PrivateRouter
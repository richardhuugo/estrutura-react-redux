
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { HomePage } from '../pages/HomePage';
import Dashboard from '../pages/dashboard'
import Auth from '../pages/auth'
import { validateToken } from '../state/actions/auth/actions'
import {   Route, Switch, Redirect } from 'react-router-dom';
import PrivateRouter from './app'
 
class  AuthOrApp extends React.Component {
    componentDidMount() {
        if (this.props.auth.user) {
            this.props.validateToken(this.props.auth.user.token)
        }
         
    }
        
    render() {
        const { user, validToken } = this.props.auth                
            return(
                <Fragment>
                  
                    {
                        user != null ?
                        
                            ( 
                        
                            <Switch>                                    
                                <PrivateRouter user={user} validToken={validToken} exact path="/"  component={HomePage} />
                                <PrivateRouter user={user} validToken={validToken} exact path="/dash"  component={Dashboard} />                                                                                               
                                <Redirect to="/" />                         
                            </Switch>
                            
                            )
                        :
                        (<Switch>                                                      
                            <Route path='/login' exact  component={Auth} />  
                            <Redirect to="/login" />                                                               
                        </Switch>)
                    }
               
                </Fragment>
               )
         
    }
}  
 
   

const mapStateToProps = state => ({ auth: state.auth })
const mapDispatchToProps = dispatch => bindActionCreators({ validateToken }, dispatch)
  

export default   (connect(mapStateToProps, mapDispatchToProps)(AuthOrApp));
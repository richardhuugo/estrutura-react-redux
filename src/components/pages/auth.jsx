
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field } from 'redux-form'
import { login, signup } from '../state/actions/auth/actions'
import {  withRouter } from 'react-router-dom';

 
 
class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = { loginMode: true }
    }

    changeMode() {
        this.setState({ loginMode: !this.state.loginMode })
    }

    onSubmit(values) {
        const { login, signup } = this.props
        this.state.loginMode ? login(values) : signup(values)
    }
    componentDidUpdate(){
        this.logged()
    }
    logged( ){
        const {user, validToken} = this.props.auth || null
         
        if(user  != null){            
            if(validToken){
                
                this.props.history.push('/dash')
            }
        }
    }
    render() {
        const { loginMode } = this.state
       const { handleSubmit ,auth} = this.props 
        return (
          
            <div className="app flex-row align-items-center">
               
                <div className="container">
                <div className="row justify-content-center">
                <div className="col-md-8">
                <div className="card-group">
                <div className="card p-4">
                <div className="card-body">
                    <form onSubmit={handleSubmit(v => this.onSubmit(v))}>
                        <h1>Login</h1>
                            <p className="text-muted">Sign In to your account</p>
                            <div className="input-group mb-3">
                            <div className="input-group-prepend">
                            <span className="input-group-text">
                            <i className="icon-user"></i>
                            </span>
                            </div>               
                            <Field  component='input' className="form-control" type="email"  name="email"
                            placeholder="E-mail" icon='envelope' />
                            </div>
                            <div className="input-group mb-4">
                            <div className="input-group-prepend">
                            <span className="input-group-text">
                            <i className="icon-lock"></i>
                            </span>
                            </div>

                        <Field component='input' className="form-control" type="password" name="senha"
                            placeholder="Senha" icon='lock' />

                            
                            </div>
                            <div className="row">
                            <div className="col-6">
                            <button type="submit" className="btn btn-primary px-4"  >Login</button>
                            </div>
                            <div className="col-6 text-right">
                            <button className="btn btn-link px-0" type="button">Forgot password?</button>
                            </div>
                            </div>
                    </form>
                      

                </div>
                </div>
                <div className="card text-white bg-primary py-5 d-md-down-none" style={space}>
                <div className="card-body text-center">
                <div>
                <h2>Sign up</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <button className="btn btn-primary active mt-3" type="button">Register Now!</button>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>

            </div>
        )
    }
}
const space = {
    width:'44%'
}
Auth = reduxForm({ form: 'authForm' ,destroyOnUnmount: false })(Auth)
const mapStateToProps = state => ({ auth: state.auth })
const mapDispatchToProps = dispatch => bindActionCreators({ login, signup }, dispatch)
export default withRouter (connect(mapStateToProps, mapDispatchToProps)(Auth)) 
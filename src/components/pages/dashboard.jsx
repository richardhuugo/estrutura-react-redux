import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'
const progress = {
    width:'25%'
}
const progress1 ={
    width:'70%'
}

  class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { path: "/dash" };
        
      }
    componentDidMount(){
       
    }
    setStatePath = (path) => {
        this.setState({path})
    }
    pathUser = (path) => {
        switch(path){
            case '/dash':
           return (
            <div>dash</div>
            )
           case '/dash/teste':
           return (
            <div>teste</div>
            )
            default:
            return (
                <div>home</div>
            )
        }
    }
    render(){
        const {auth} = this.props
        const {path} = this.state
        return(
        
           <main className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show" >              
            <header className="app-header navbar">
                <button className="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
                <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand" href="#">
                <img className="navbar-brand-full" src="img/brand/logo.svg" width="89" height="25" alt="CoreUI Logo" />
                <img className="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo" />
                </a>
                <button className="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
                <span className="navbar-toggler-icon"></span>
                </button>
                <ul className="nav navbar-nav d-md-down-none">
                <li className="nav-item px-3">
                
                <a className="nav-link" href="#" onClick={()=>{this.setStatePath('/dash')}} >Dashboard</a>
                </li>
                <li className="nav-item px-3">
                <a className="nav-link" href="#" onClick={()=>{this.setStatePath('/dash/teste')}}   >teste</a>
                </li>
                <li className="nav-item px-3">
                <a className="nav-link" href="#">Settings</a>
                </li>
                </ul>
                <ul className="nav navbar-nav ml-auto">
                <li className="nav-item dropdown d-md-down-none">
                <a className="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i className="icon-bell"></i>
                <span className="badge badge-pill badge-danger">5</span>
                </a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div className="dropdown-header text-center">
                <strong>You have 5 notifications</strong>
                </div>
                <a className="dropdown-item" href="#">
                <i className="icon-user-follow text-success"></i> New user registered</a>
                <a className="dropdown-item" href="#">
                <i className="icon-user-unfollow text-danger"></i> User deleted</a>
                <a className="dropdown-item" href="#">
                <i className="icon-chart text-info"></i> Sales report is ready</a>
                <a className="dropdown-item" href="#">
                <i className="icon-basket-loaded text-primary"></i> New client</a>
                <a className="dropdown-item" href="#">
                <i className="icon-speedometer text-warning"></i> Server overloaded</a>
                <div className="dropdown-header text-center">
                <strong>Server</strong>
                </div>
                <a className="dropdown-item" href="#">
                <div className="text-uppercase mb-1">
                <small>
                <b>CPU Usage</b>
                </small>
                </div>
                <span className="progress progress-xs">
                <div className="progress-bar bg-info" role="progressbar" style={progress}  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </span>
                <small className="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a className="dropdown-item" href="#">
                <div className="text-uppercase mb-1">
                <small>
                <b>Memory Usage</b>
                </small>
                </div>
                <span className="progress progress-xs">
                <div className="progress-bar bg-warning" role="progressbar" style={progress1} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                </span>
                <small className="text-muted">11444GB/16384MB</small>
                </a>
            
                </div>
                </li>
                <li className="nav-item dropdown d-md-down-none">
                <a className="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i className="icon-list"></i>
                <span className="badge badge-pill badge-warning">15</span>
                </a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div className="dropdown-header text-center">
                <strong>You have 5 pending tasks</strong>
                </div>
                <a className="dropdown-item" href="#">
                <div className="small mb-1">Upgrade NPM &amp; Bower
                <span className="float-right">
                <strong>0%</strong>
                </span>
                </div>
                <span className="progress progress-xs">
                <div className="progress-bar bg-info" role="progressbar"   aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </span>
                </a>
            
            
                <a className="dropdown-item text-center" href="#">
                <strong>View all tasks</strong>
                </a>
                </div>
                </li>
                <li className="nav-item dropdown d-md-down-none">
                <a className="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i className="icon-envelope-letter"></i>
                <span className="badge badge-pill badge-info">7</span>
                </a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div className="dropdown-header text-center">
                <strong>You have 4 messages</strong>
                </div>
                <a className="dropdown-item" href="#">
                <div className="message">
                <div className="py-3 mr-3 float-left">
                <div className="avatar">
                <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                <span className="avatar-status badge-success"></span>
                </div>
                </div>
                <div>
                <small className="text-muted">John Doe</small>
                <small className="text-muted float-right mt-1">Just now</small>
                </div>
                <div className="text-truncate font-weight-bold">
                <span className="fa fa-exclamation text-danger"></span> Important message</div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                </div>
                </a>
                <a className="dropdown-item" href="#">
                <div className="message">
                <div className="py-3 mr-3 float-left">
                <div className="avatar">
                <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com" />
                <span className="avatar-status badge-warning"></span>
                </div>
                </div>
                <div>
                <small className="text-muted">John Doe</small>
                <small className="text-muted float-right mt-1">5 minutes ago</small>
                </div>
                <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                </div>
                </a>
                <a className="dropdown-item" href="#">
                <div className="message">
                <div className="py-3 mr-3 float-left">
                <div className="avatar">
                <img className="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com" />
                <span className="avatar-status badge-danger"></span>
                </div>
                </div>
                <div>
                <small className="text-muted">John Doe</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
                </div>
                <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                </div>
                </a>
                <a className="dropdown-item" href="#">
                <div className="message">
                <div className="py-3 mr-3 float-left">
                <div className="avatar">
                <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                <span className="avatar-status badge-info"></span>
                </div>
                </div>
                <div>
                <small className="text-muted">John Doe</small>
                <small className="text-muted float-right mt-1">4:03 PM</small>
                </div>
                <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                </div>
                </a>
                <a className="dropdown-item text-center" href="#">
                <strong>View all messages</strong>
                </a>
                </div>
                </li>
                <li className="nav-item dropdown">
                <a className="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com" />
                </a>
                <div className="dropdown-menu dropdown-menu-right">
                <div className="dropdown-header text-center">
                <strong>Account</strong>
                </div>
                <a className="dropdown-item" href="#">
                <i className="fa fa-bell-o"></i> Updates
                <span className="badge badge-info">42</span>
                </a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-envelope-o"></i> Messages
                <span className="badge badge-success">42</span>
                </a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-tasks"></i> Tasks
                <span className="badge badge-danger">42</span>
                </a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-comments"></i> Comments
                <span className="badge badge-warning">42</span>
                </a>
                <div className="dropdown-header text-center">
                <strong>Settings</strong>
                </div>
                <a className="dropdown-item" href="#">
                <i className="fa fa-user"></i> Profile</a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-wrench"></i> Settings</a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-usd"></i> Payments
                <span className="badge badge-dark">42</span>
                </a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-file"></i> Projects
                <span className="badge badge-primary">42</span>
                </a>
                <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#">
                <i className="fa fa-shield"></i> Lock Account</a>
                <a className="dropdown-item" href="#">
                <i className="fa fa-lock"></i> Logout</a>
                </div>
                </li>
                </ul>
                <button className="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
                <span className="navbar-toggler-icon"></span>
                </button>
                <button className="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
                <span className="navbar-toggler-icon"></span>
                </button>
            </header>
        
                <div className="app-body">
                    <div className="sidebar">
                    <nav className="sidebar-nav ps ps--active-y">
                    <ul className="nav">
                    <li className="nav-item">
                    <a className="nav-link active" href="main.html">
                    <i className="nav-icon icon-speedometer"></i> Dashboard
                    <span className="badge badge-info">NEW</span>
                    </a>
                    </li>
                    <li className="nav-title">Theme</li>
               
                    <li className="nav-item">
                    <a className="nav-link" href="typography.html">
                    <i className="nav-icon icon-pencil"></i> Typography</a>
                    </li>
                    <li className="nav-title">Funcoes</li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-puzzle"></i> Funcionarios</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="base/breadcrumb.html">
                    <i className="nav-icon icon-puzzle"></i> Registrar</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="base/cards.html">
                    <i className="nav-icon icon-puzzle"></i> Cards</a>
                    </li>
                    
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-cursor"></i> Buttons</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="buttons/buttons.html">
                    <i className="nav-icon icon-cursor"></i> Buttons</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="buttons/brand-buttons.html">
                    <i className="nav-icon icon-cursor"></i> Brand Buttons</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="buttons/button-group.html">
                    <i className="nav-icon icon-cursor"></i> Buttons Group</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="buttons/dropdowns.html">
                    <i className="nav-icon icon-cursor"></i> Dropdowns</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="buttons/loading-buttons.html">
                    <i className="nav-icon icon-cursor"></i> Loading Buttons
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="charts.html">
                    <i className="nav-icon icon-pie-chart"></i> Charts</a>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon fa fa-code"></i> Editors</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="editors/code-editor.html">
                    <i className="nav-icon icon-note"></i> Code Editor
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="editors/markdown-editor.html">
                    <i className="nav-icon fa fa-code"></i> Markdown
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="editors/text-editor.html">
                    <i className="nav-icon icon-note"></i> Rich Text Editor
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-note"></i> Forms</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="forms/basic-forms.html">
                    <i className="nav-icon icon-note"></i> Basic Forms</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="forms/advanced-forms.html">
                    <i className="nav-icon icon-note"></i> Advanced
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="forms/validation.html">
                    <i className="nav-icon icon-note"></i> Validation
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="google-maps.html">
                    <i className="nav-icon icon-map"></i> Google Maps
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-star"></i> Icons</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="icons/coreui-icons.html">
                    <i className="nav-icon icon-star"></i> CoreUI Icons
                    <span className="badge badge-success">NEW</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="icons/flags.html">
                    <i className="nav-icon icon-star"></i> Flags</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="icons/font-awesome.html">
                    <i className="nav-icon icon-star"></i> Font Awesome
                    <span className="badge badge-secondary">4.7</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="icons/simple-line-icons.html">
                    <i className="nav-icon icon-star"></i> Simple Line Icons</a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-bell"></i> Notifications</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="notifications/alerts.html">
                    <i className="nav-icon icon-bell"></i> Alerts</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="notifications/badge.html">
                    <i className="nav-icon icon-bell"></i> Badge</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="notifications/modals.html">
                    <i className="nav-icon icon-bell"></i> Modals</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="notifications/toastr.html">
                    <i className="nav-icon icon-bell"></i> Toastr
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-energy"></i> Plugins</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="plugins/calendar.html">
                    <i className="nav-icon icon-calendar"></i> Calendar
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="plugins/draggable-cards.html">
                    <i className="nav-icon icon-cursor-move"></i> Draggable
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="plugins/sliders.html">
                    <i className="nav-icon icon-equalizer"></i> Sliders
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="plugins/spinners.html">
                    <i className="nav-icon fa fa-spinner"></i> Spinners
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-list"></i> Tables</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="tables/tables.html">
                    <i className="nav-icon icon-list"></i> Standard Tables</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="tables/datatables.html">
                    <i className="nav-icon icon-list"></i> DataTables
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="widgets.html">
                    <i className="nav-icon icon-calculator"></i> Widgets
                    <span className="badge badge-info">NEW</span>
                    </a>
                    </li>
                    <li className="nav-divider"></li>
                    <li className="nav-title">Extras</li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-star"></i> Pages</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="login.html" target="_top">
                    <i className="nav-icon icon-star"></i> Login</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="register.html" target="_top">
                    <i className="nav-icon icon-star"></i> Register</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="404.html" target="_top">
                    <i className="nav-icon icon-star"></i> Error 404</a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="500.html" target="_top">
                    <i className="nav-icon icon-star"></i> Error 500</a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-layers"></i> Apps</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-speech"></i> Invoicing</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="apps/invoicing/invoice.html">
                    <i className="nav-icon icon-speech"></i> Invoice
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-item nav-dropdown">
                    <a className="nav-link nav-dropdown-toggle" href="#">
                    <i className="nav-icon icon-speech"></i> Email</a>
                    <ul className="nav-dropdown-items">
                    <li className="nav-item">
                    <a className="nav-link" href="apps/email/inbox.html">
                    <i className="nav-icon icon-speech"></i> Inbox
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="apps/email/message.html">
                    <i className="nav-icon icon-speech"></i> Message
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="apps/email/compose.html">
                    <i className="nav-icon icon-speech"></i> Compose
                    <span className="badge badge-danger">PRO</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    <li className="nav-divider"></li>
                    <li className="nav-title">Labels</li>
                    <li className="nav-item d-compact-none d-minimized-none">
                    <a className="nav-label" href="#">
                    <i className="fa fa-circle text-danger"></i> Label danger</a>
                    </li>
                    <li className="nav-item d-compact-none d-minimized-none">
                    <a className="nav-label" href="#">
                    <i className="fa fa-circle text-info"></i> Label info</a>
                    </li>
                    <li className="nav-item d-compact-none d-minimized-none">
                    <a className="nav-label" href="#">
                    <i className="fa fa-circle text-warning"></i> Label warning</a>
                    </li>
                    <li className="nav-divider"></li>
                    <li className="nav-title">System Utilization</li>
                    <li className="nav-item px-3 d-compact-none d-minimized-none">
                    <div className="text-uppercase mb-1">
                    <small>
                    <b>CPU Usage</b>
                    </small>
                    </div>
                    <div className="progress progress-xs">
                    <div className="progress-bar bg-info" role="progressbar" style={progress} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <small className="text-muted">348 Processes. 1/4 Cores.</small>
                    </li>
                   
                    </ul>
                    <div className="ps__rail-x" style={side}><div className="ps__thumb-x" tabIndex="0"  ></div></div><div className="ps__rail-y" style={ind}><div className="ps__thumb-y" tabIndex="0" style={next}></div></div></nav>
                    
                    </div>


                    <main className="main">
                    {this.pathUser(path)}
                    </main>


                    <aside className="aside-menu">
                        <ul className="nav nav-tabs" role="tablist">
                        <li className="nav-item">
                        <a className="nav-link active" data-toggle="tab" href="#timeline" role="tab">
                        <i className="icon-list"></i>
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#messages" role="tab">
                        <i className="icon-speech"></i>
                        </a>
                        </li>
                        <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#settings" role="tab">
                        <i className="icon-settings"></i>
                        </a>
                        </li>
                        </ul>

                        <div className="tab-content">
                        <div className="tab-pane active" id="timeline" role="tabpanel">
                        <div className="list-group list-group-accent">
                        <div className="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Today</div>
                        <div className="list-group-item list-group-item-accent-warning list-group-item-divider">
                        <div className="avatar float-right">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div>Meeting with
                        <strong>Lucas</strong>
                        </div>
                        <small className="text-muted mr-3">
                        <i className="icon-calendar"></i>&nbsp; 1 - 3pm</small>
                        <small className="text-muted">
                        <i className="icon-location-pin"></i>&nbsp; Palo Alto, CA</small>
                        </div>
                        <div className="list-group-item list-group-item-accent-info">
                        <div className="avatar float-right">
                        <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div>Skype with
                        <strong>Megan</strong>
                        </div>
                        <small className="text-muted mr-3">
                        <i className="icon-calendar"></i>&nbsp; 4 - 5pm</small>
                        <small className="text-muted">
                        <i className="icon-social-skype"></i>&nbsp; On-line</small>
                        </div>
                        <div className="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Tomorrow</div>
                        <div className="list-group-item list-group-item-accent-danger list-group-item-divider">
                        <div>New UI Project -
                        <strong>deadline</strong>
                        </div>
                        <small className="text-muted mr-3">
                        <i className="icon-calendar"></i>&nbsp; 10 - 11pm</small>
                        <small className="text-muted">
                        <i className="icon-home"></i>&nbsp; creativeLabs HQ</small>
                        <div className="avatars-stack mt-2">
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/2.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/3.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        </div>
                        </div>
                        <div className="list-group-item list-group-item-accent-success list-group-item-divider">
                        <div>
                        <strong>#10 Startups.Garden</strong> Meetup</div>
                        <small className="text-muted mr-3">
                        <i className="icon-calendar"></i>&nbsp; 1 - 3pm</small>
                        <small className="text-muted">
                        <i className="icon-location-pin"></i>&nbsp; Palo Alto, CA</small>
                        </div>
                        <div className="list-group-item list-group-item-accent-primary list-group-item-divider">
                        <div>
                        <strong>Team meeting</strong>
                        </div>
                        <small className="text-muted mr-3">
                        <i className="icon-calendar"></i>&nbsp; 4 - 6pm</small>
                        <small className="text-muted">
                        <i className="icon-home"></i>&nbsp; creativeLabs HQ</small>
                        <div className="avatars-stack mt-2">
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/2.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/3.jpg" alt="admin@bootstrapmaster.com" />
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com"/>
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com"/>
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com"/>
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        </div>
                        <div className="avatar avatar-xs">
                        <img className="img-avatar" src="img/avatars/8.jpg" alt="admin@bootstrapmaster.com"/>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        <div className="tab-pane p-3" id="messages" role="tabpanel">
                        <div className="message">
                        <div className="py-3 pb-5 mr-3 float-left">
                        <div className="avatar">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        <span className="avatar-status badge-success"></span>
                        </div>
                        </div>
                        <div>
                        <small className="text-muted">Lukasz Holeczek</small>
                        <small className="text-muted float-right mt-1">1:52 PM</small>
                        </div>
                        <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
                        </div>
                        <hr/>
                        <div className="message">
                        <div className="py-3 pb-5 mr-3 float-left">
                        <div className="avatar">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        <span className="avatar-status badge-success"></span>
                        </div>
                        </div>
                        <div>
                        <small className="text-muted">Lukasz Holeczek</small>
                        <small className="text-muted float-right mt-1">1:52 PM</small>
                        </div>
                        <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
                        </div>
                        <hr/>
                        <div className="message">
                        <div className="py-3 pb-5 mr-3 float-left">
                        <div className="avatar">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        <span className="avatar-status badge-success"></span>
                        </div>
                        </div>
                        <div>
                        <small className="text-muted">Lukasz Holeczek</small>
                        <small className="text-muted float-right mt-1">1:52 PM</small>
                        </div>
                        <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
                        </div>
                        <hr/>
                        <div className="message">
                        <div className="py-3 pb-5 mr-3 float-left">
                        <div className="avatar">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        <span className="avatar-status badge-success"></span>
                        </div>
                        </div>
                        <div>
                        <small className="text-muted">Lukasz Holeczek</small>
                        <small className="text-muted float-right mt-1">1:52 PM</small>
                        </div>
                        <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
                        </div>
                        <hr/>
                        <div className="message">
                        <div className="py-3 pb-5 mr-3 float-left">
                        <div className="avatar">
                        <img className="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com"/>
                        <span className="avatar-status badge-success"></span>
                        </div>
                        </div>
                        <div>
                        <small className="text-muted">Lukasz Holeczek</small>
                        <small className="text-muted float-right mt-1">1:52 PM</small>
                        </div>
                        <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
                        </div>
                        </div>
                        <div className="tab-pane p-3" id="settings" role="tabpanel">
                        <h6>Settings</h6>
                        <div className="aside-options">
                        <div className="clearfix mt-4">
                        <small>
                        <b>Option 1</b>
                        </small>
                        <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                        <input className="switch-input" type="checkbox" checked=""/>
                        <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                        </label>
                        </div>
                        <div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
                        </div>
                        </div>
                        <div className="aside-options">
                        <div className="clearfix mt-3">
                        <small>
                        <b>Option 2</b>
                        </small>
                        <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                        <input className="switch-input" type="checkbox"/>
                        <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                        </label>
                        </div>
                        <div>
                        <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
                        </div>
                        </div>
                        <div className="aside-options">
                        <div className="clearfix mt-3">
                        <small>
                        <b>Option 3</b>
                        </small>
                        <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                        <input className="switch-input" type="checkbox"/>
                        <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                        </label>
                        </div>
                        </div>
                        <div className="aside-options">
                        <div className="clearfix mt-3">
                        <small>
                        <b>Option 4</b>
                        </small>
                        <label className="switch switch-label switch-pill switch-success switch-sm float-right">
                        <input className="switch-input" type="checkbox" checked=""/>
                        <span className="switch-slider" data-checked="On" data-unchecked="Off"></span>
                        </label>
                        </div>
                        </div>
                        <hr/>
                        <h6>System Utilization</h6>
                        <div className="text-uppercase mb-1 mt-4">
                        <small>
                        <b>CPU Usage</b>
                        </small>
                        </div>
                        <div className="progress progress-xs">
                        <div className="progress-bar bg-info" role="progressbar" style={progress} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <small className="text-muted">348 Processes. 1/4 Cores.</small>
                        <div className="text-uppercase mb-1 mt-2">
                        <small>
                        <b>Memory Usage</b>
                        </small>
                        </div>
                      
                      
                      
                     
                      
                        </div>
                        </div>
                        </aside>
                </div>
           </main>
        )
    }
}


const mapStateToProps = state => ({ auth: state.auth })
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)
export default     (connect(mapStateToProps, mapDispatchToProps)(Dashboard)) 

const side ={
    left:'0px',
    bottom:'0px'
}

const ind = {
    top:' 0px ',
    height: '479px',
     right: '0px'
}
const next ={
    top: '0px',
     height: '193px'
}
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/App';
import 'jquery'
import 'bootstrap'
import 'popper.js'
import '@coreui/coreui'
import '@coreui/coreui/dist/js/coreui.min'
import '@coreui/coreui/dist/css/coreui.min.css'

   
ReactDOM.render(<App />, document.getElementById('app'));